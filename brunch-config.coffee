# http://brunch.io/docs/config
module.exports =
  conventions:
    assets: 'app/static/**'
  files:
    javascripts:
      joinTo:
        'app.js': 'app/**'
        'vendor.js': ['node_modules/**', 'vendor/**']
    stylesheets:
      joinTo: 'app.css'
  modules:
    autoRequire:
      'app.js': ['initialize']
  npm:
    static: [
      'vendor/phaser.min.js',
      'vendor/jquery.min.js',
      'vendor/jqueryplugin.lettering.js',
      'vendor/jqueryplugin.textillate.js',
      'vendor/moment.min.js',
      'vendor/sweetalert2.min.js',
      'vendor/color-scheme.min.js'
      # 'node_modules/phaser/dist/phaser-arcade-physics.js'
    ]
  plugins:
    # https://github.com/babel/babel-brunch#configuration
    babel:
      ignore: ['node_modules/**', 'vendor/**']
  server:
    noPushState: on
