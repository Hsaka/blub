import Utils from 'utils/utils';

export default class MainScene extends Phaser.Scene {
  constructor() {
    super('main');
  }

  init(data) {
    this.initData = data;
  }

  setup() {
    if (!Utils.GlobalSettings.bgm.isPlaying) {
      Utils.GlobalSettings.bgm.setVolume(0.2);
      Utils.GlobalSettings.bgm.setLoop(true);
      Utils.GlobalSettings.bgm.play();
    }

    if (Utils.SavedSettings.muted) {
      Utils.GlobalSettings.bgm.pause();
    }

    this.bumpSnd = this.sound.add('bump');
    this.hitSnd = this.sound.add('hit');
    this.eatSnd = this.sound.add('eat');
    this.growSnd = this.sound.add('grow');

    this.screenGroup = this.add.container(0, 0);

    this.bg = this.add.image(0, 0, 'bg');
    this.bg.setOrigin(0, 0);
    this.screenGroup.add(this.bg);

    this.map = this.make.tilemap({ key: 'map' });

    // The first parameter is the name of the tileset in Tiled and the second parameter is the key
    // of the tileset image used when loading the file in preload.
    var terrain = this.map.addTilesetImage('LPC Basic Terrain', 'terrain');
    var obstacles = this.map.addTilesetImage('LPC Obstacles', 'obstacles');

    // You can load a layer from the map using the layer name from Tiled, or by using the layer
    // index (0 in this case).
    this.backgroundLayer = this.map.createStaticLayer(0, terrain, 0, 0);
    this.highGrassLayer = this.map.createStaticLayer(1, terrain, 0, 0);
    this.obstacleLayer = this.map.createStaticLayer(2, obstacles, 0, 0);

    this.currentZoom = 32;

    this.cameras.main.setBounds(
      0,
      0,
      this.map.widthInPixels,
      this.map.heightInPixels
    );
    this.cameras.main.setZoom(this.currentZoom);
    this.cameras.main.centerOn(0, 0);

    this.playerScale = 1 / (this.currentZoom * 2); //0.02;
    this.playerHorizontalDirection = 0;
    this.playerVerticalDirection = 0;
    this.playerFoodEaten = 0;
    this.edibleObjects = [];
    this.numEdibles = 59;
    this.numEnemies = 1;
    this.maxEdibles = 20;
    this.maxEnemies = 100;
    this.maxWorldEdibles = 500;
    this.maxWorldEnemies = 100;
    this.hsv = Phaser.Display.Color.HSVColorWheel();

    this.edibleGroup = this.physics.add.group();
    for (var i = 0; i < this.maxEdibles; i++) {
      var spr = this.edibleGroup.create(0, 0, 'atlas1', 'plants1');
      spr.setScale(this.playerScale * 1.5);
      spr.disableBody(true, true);
      spr._edibleObj = undefined;
      this.edibleGroup.killAndHide(spr);
    }

    this.enemyGroup = this.physics.add.group();
    for (var i = 0; i < this.maxEnemies; i++) {
      var spr = this.enemyGroup.create(0, 0, 'atlas1', 'blub2');
      // spr.setCircle(spr.width / 2);
      spr.setSize(spr.width / 1.5, spr.height / 1.5);
      spr.setScale(this.playerScale);
      spr.disableBody(true, true);
      spr._ai = {
        state: 0,
        timer: 0
      };
      this.enemyGroup.killAndHide(spr);
    }

    this.player = this.physics.add.sprite(16 * 8, 16 * 4, 'atlas1', 'blub1');
    this.player.setOrigin(0.5, 0.5);
    // this.player.setCircle(this.player.width / 2);
    this.player.setSize(this.player.width / 1.5, this.player.height / 1.5);
    this.player.setScale(this.playerScale);
    this.player.setInteractive();
    this.playerEyes = this.add.image(
      this.player.x,
      this.player.y - 0.1,
      'atlas1',
      'eyes'
    );
    this.playerEyes.setScale(this.playerScale);
    this.playerEyesDirection = {
      x: 0,
      y: -0.1
    };

    this.physics.add.overlap(
      this.player,
      this.edibleGroup,
      this.hitEdible,
      null,
      this
    );

    this.physics.add.overlap(
      this.player,
      this.enemyGroup,
      this.hitEnemy,
      null,
      this
    );

    this.generateEdibles();
    this.generateEnemies();

    this.canvasTexture = this.textures.createCanvas('canvasTexture', 16, 16);
    this.canvasPixelColor = new Phaser.Display.Color();

    this.baseTileId = 529;

    this.tweens.add({
      targets: this.player,
      props: {
        scaleX: { value: this.playerScale - 0.002, duration: 1000 },
        scaleY: { value: this.playerScale - 0.005, duration: 1000 },
        angle: { value: 5, duration: 500, delay: 250 }
      },
      ease: 'Sine.easeInOut',
      repeat: -1,
      yoyo: true
    });

    this.cameras.main.startFollow(this.player);

    this.isMoving = false;

    this.player.on('pointerdown', this.pukeFood, this);

    this.input.on('pointerup', pointer => {
      this.isMoving = false;
      this.player.body.stop();
      this.playerEyesDirection.x = 0;
      this.playerEyesDirection.y = -0.1;
      this.playerHorizontalDirection = 0;
      this.playerVerticalDirection = 0;

      this.tweens.killTweensOf(this.player);
      this.player.setScale(this.playerScale);
      this.player.angle = 0;

      this.tweens.add({
        targets: this.player,
        props: {
          scaleX: { value: this.playerScale - 0.002, duration: 1000 },
          scaleY: { value: this.playerScale - 0.005, duration: 1000 },
          angle: { value: 5, duration: 500, delay: 250 }
        },
        ease: 'Sine.easeInOut',
        repeat: -1,
        yoyo: true
      });
    });

    this.input.on(
      'pointermove',
      pointer => {
        if (pointer.isDown) {
          this.movePlayer();
        }
      },
      this
    );

    this.input.on(
      'pointerdown',
      pointer => {
        this.movePlayer();
      },
      this
    );

    // this.input.keyboard.on('keydown-Z', event => {
    //   if (this.currentZoom > 1) {
    //     this.currentZoom--;
    //   }
    //   this.cameras.main.setZoom(this.currentZoom);
    // });

    // this.input.keyboard.on('keydown-X', event => {
    //   if (this.currentZoom < 32) {
    //     this.currentZoom++;
    //   }
    //   this.cameras.main.setZoom(this.currentZoom);
    // });

    if (this.scene.isSleeping('gameUI')) {
      this.scene.wake('gameUI', { showHome: true });
      this.scene.bringToTop('gameUI');
    } else if (!this.scene.isActive('gameUI')) {
      this.scene.launch('gameUI', { showHome: true });
      this.scene.bringToTop('gameUI');
    }
    this.registry.set('showHome', true);
    this.registry.set('setMessage', '');

    // this.graphics = this.add.graphics({
    //   lineStyle: { width: 2, color: 0x00ff00 },
    //   fillStyle: { color: 0xff0000 }
    // });
    this.visionCircle = new Phaser.Geom.Circle(
      this.player.x,
      this.player.y,
      400 / this.currentZoom
    );
  }

  generateEdibles() {
    var scaleX = Utils.GlobalSettings.width / this.currentZoom;
    var scaleY = Utils.GlobalSettings.height / this.currentZoom;

    for (var i = 0; i < this.maxWorldEdibles; i++) {
      var inObstacle = true;
      while (inObstacle) {
        var xp = Phaser.Math.RND.integerInRange(2, this.map.widthInPixels - 2);
        var yp = Phaser.Math.RND.integerInRange(2, this.map.heightInPixels - 2);

        if (i < 50) {
          xp = Phaser.Math.RND.integerInRange(
            this.player.x - scaleX * 7,
            this.player.x + scaleX * 7
          );
          yp = Phaser.Math.RND.integerInRange(
            this.player.y - scaleY * 7,
            this.player.y + scaleY * 7
          );
        }

        var tileX = this.map.worldToTileX(xp);
        var tileY = this.map.worldToTileY(yp);

        var tile = this.map.getTileAt(tileX, tileY);
        if (!tile) {
          this.edibleObjects.push({
            x: xp,
            y: yp,
            frame: Phaser.Math.RND.integerInRange(1, this.numEdibles),
            type: 'food',
            onScreen: false,
            sprite: undefined,
            consumed: false
          });
          inObstacle = false;
        } else {
          inObstacle = true;
        }
      }
    }

    this.edibleObjects.push({
      x: Phaser.Math.RND.integerInRange(
        this.player.x - scaleX / 3,
        this.player.x + scaleX / 3
      ),
      y: Phaser.Math.RND.integerInRange(
        this.player.y - scaleY / 3,
        this.player.y + scaleY / 3
      ),
      frame: Phaser.Math.RND.integerInRange(1, this.numEdibles),
      type: 'food',
      onScreen: false,
      sprite: undefined,
      consumed: false
    });

    // for (var i = 0; i < this.maxWorldEnemies; i++) {
    //   this.edibleObjects.push({
    //     x: Phaser.Math.RND.integerInRange(2, Utils.GlobalSettings.width - 2),
    //     y: Phaser.Math.RND.integerInRange(2, Utils.GlobalSettings.height - 2),
    //     color: this.hsv[Phaser.Math.RND.integerInRange(1, 360)].color,
    //     type: 'enemy',
    //     size: 1 / (Phaser.Math.RND.integerInRange(1, 32) * 2),
    //     onScreen: false,
    //     sprite: undefined,
    //     consumed: false
    //   });
    // }
  }

  generateEnemies() {
    for (var i = 0; i < this.maxWorldEnemies; i++) {
      var enemy = this.enemyGroup.getFirstDead();
      if (enemy) {
        enemy.enableBody(true, 0, 0, true, true);
        enemy.x = Phaser.Math.RND.integerInRange(
          this.player.x + 20,
          this.map.widthInPixels - 2
        );
        enemy.y = Phaser.Math.RND.integerInRange(
          2,
          this.map.heightInPixels - 2
        );
        enemy.setScale(1 / (Phaser.Math.RND.integerInRange(1, 32) * 2));
        enemy.setFrame('blub2');
        enemy.setTint(this.hsv[Phaser.Math.RND.integerInRange(0, 359)].color);
      }
    }
  }

  handlePlayerVisibility() {
    this.visionCircle.setTo(
      this.player.x,
      this.player.y,
      400 / this.currentZoom
    );

    // this.graphics.clear();
    // this.graphics.strokeCircleShape(this.visionCircle);

    for (var i = 0; i < this.edibleObjects.length; i++) {
      var edibleObj = this.edibleObjects[i];
      if (!edibleObj.consumed) {
        if (
          Phaser.Geom.Circle.Contains(
            this.visionCircle,
            edibleObj.x,
            edibleObj.y
          )
        ) {
          if (!edibleObj.onScreen) {
            edibleObj.onScreen = true;

            if (edibleObj.type === 'food') {
              var edible = this.edibleGroup.getFirstDead();
              if (edible) {
                edible.enableBody(true, 0, 0, true, true);
                edible.x = edibleObj.x;
                edible.y = edibleObj.y;
                edible.setScale(
                  this.playerScale * Phaser.Math.RND.realInRange(1.5, 1.8)
                );
                edible.setFrame('plants' + edibleObj.frame);
                edibleObj.sprite = edible;
                edible._edibleObj = edibleObj;
              }
            }
          }
          // else if (edibleObj.type === 'enemy') {
          //   var enemy = this.enemyGroup.getFirstDead();
          //   if (enemy) {
          //     enemy.enableBody(true, 0, 0, true, true);
          //     enemy.x = edibleObj.x;
          //     enemy.y = edibleObj.y;
          //     enemy.setScale(edibleObj.size);
          //     enemy.setFrame('blub2');
          //     enemy.setTint(edibleObj.color);
          //     edibleObj.sprite = enemy;
          //     enemy._edibleObj = edibleObj;
          //   }
          // }
        } else {
          if (edibleObj.onScreen && edibleObj.sprite) {
            edibleObj.sprite.disableBody(true, true);
            this.edibleGroup.killAndHide(edibleObj.sprite);
          }
          edibleObj.onScreen = false;
        }
      }
    }
  }

  hitEnemy(player, enemy) {
    if (this.playerScale >= enemy.scaleX) {
      if (!Utils.SavedSettings.muted) {
        this.eatSnd.play();
      }

      this.eatFood(player, enemy);

      enemy.disableBody(true, true);
      this.enemyGroup.killAndHide(enemy);
    } else {
      if (!Utils.SavedSettings.muted) {
        this.hitSnd.play();
      }

      player.disableBody(true, false);
      this.tweens.killTweensOf(this.player);

      this.registry.set('setMessage', 'Game Over!');

      this.tweens.add({
        targets: [this.player, this.playerEyes],
        props: {
          scaleX: { value: 0, duration: 1000 },
          scaleY: { value: 0, duration: 1000 },
          angle: { value: 720, duration: 500, delay: 250 }
        },
        ease: 'Back.easeOut',
        onComplete: () => {
          this.player.visible = false;
          this.playerEyes.visible = false;
        }
      });
    }
  }

  pukeFood() {
    if (this.playerFoodEaten > 0) {
      this.playerFoodEaten--;
      this.player.clearTint();

      this.tweens.addCounter({
        from: 0,
        to: 100,
        duration: 500,
        onUpdate: tween => {
          var value = Math.floor(tween.getValue());
          var tintValue = Math.ceil(value / 10);
          if (tintValue % 2 === 0) {
            this.player.clearTint();
          } else {
            this.player.setTintFill(0xff0000);
          }
        },
        onComplete: () => {
          this.player.clearTint();

          if (this.playerFoodEaten % 1 === 0) {
            if (this.currentZoom < 32) {
              if (!Utils.SavedSettings.muted) {
                this.hitSnd.play();
              }

              this.currentZoom++;
              this.cameras.main.zoomTo(this.currentZoom, 1000);
              // this.player.setScale(this.playerScale);

              this.playerScale = 1 / (this.currentZoom * 2);

              this.tweens.killTweensOf(this.player);

              this.tweens.add({
                targets: [this.player, this.playerEyes],
                props: {
                  scaleX: { value: this.playerScale, duration: 1000 },
                  scaleY: { value: this.playerScale, duration: 1000 }
                },
                ease: 'Elastic.easeOut',
                onComplete: () => {
                  this.tweens.killTweensOf(this.player);
                  this.player.setScale(this.playerScale);
                  this.playerEyes.setScale(this.playerScale);
                  this.player.angle = 0;

                  if (this.isMoving) {
                    this.tweens.add({
                      targets: this.player,
                      props: {
                        scaleX: {
                          value: this.playerScale - 0.002,
                          duration: 150
                        },
                        scaleY: {
                          value: this.playerScale - 0.005,
                          duration: 150
                        },
                        angle: { value: 15, duration: 200, delay: 50 }
                      },
                      ease: 'Sine.easeInOut',
                      repeat: -1,
                      yoyo: true
                    });
                  } else {
                    this.tweens.add({
                      targets: this.player,
                      props: {
                        scaleX: {
                          value: this.playerScale - 0.002,
                          duration: 1000
                        },
                        scaleY: {
                          value: this.playerScale - 0.005,
                          duration: 1000
                        },
                        angle: { value: 5, duration: 500, delay: 250 }
                      },
                      ease: 'Sine.easeInOut',
                      repeat: -1,
                      yoyo: true
                    });
                  }
                }
              });
            }
          }
        }
      });
    }
  }

  eatFood(player, edible) {
    this.playerFoodEaten++;
    this.player.clearTint();

    this.tweens.addCounter({
      from: 0,
      to: 100,
      duration: 500,
      onUpdate: tween => {
        var value = Math.floor(tween.getValue());
        var tintValue = Math.ceil(value / 10);
        if (tintValue % 2 === 0) {
          this.player.clearTint();
        } else {
          if (this.playerFoodEaten % 3 === 0) {
            this.player.setTintFill(0xffff00, 0xffff00, 0xff0000, 0xff0000);
          } else {
            this.player.setTintFill(0xffff00);
          }
        }
      },
      onComplete: () => {
        this.player.clearTint();

        if (this.playerFoodEaten % 1 === 0) {
          if (this.currentZoom > 1) {
            if (!Utils.SavedSettings.muted) {
              this.growSnd.play();
            }

            this.currentZoom--;
            this.cameras.main.zoomTo(this.currentZoom, 1000);
            // this.player.setScale(this.playerScale);

            this.playerScale = 1 / (this.currentZoom * 2);

            this.tweens.killTweensOf(this.player);

            this.tweens.add({
              targets: [this.player, this.playerEyes],
              props: {
                scaleX: { value: this.playerScale, duration: 1000 },
                scaleY: { value: this.playerScale, duration: 1000 }
              },
              ease: 'Elastic.easeOut',
              onComplete: () => {
                this.tweens.killTweensOf(this.player);
                this.player.setScale(this.playerScale);
                this.playerEyes.setScale(this.playerScale);
                this.player.angle = 0;

                if (this.isMoving) {
                  this.tweens.add({
                    targets: this.player,
                    props: {
                      scaleX: {
                        value: this.playerScale - 0.002,
                        duration: 150
                      },
                      scaleY: {
                        value: this.playerScale - 0.005,
                        duration: 150
                      },
                      angle: { value: 15, duration: 200, delay: 50 }
                    },
                    ease: 'Sine.easeInOut',
                    repeat: -1,
                    yoyo: true
                  });
                } else {
                  this.tweens.add({
                    targets: this.player,
                    props: {
                      scaleX: {
                        value: this.playerScale - 0.002,
                        duration: 1000
                      },
                      scaleY: {
                        value: this.playerScale - 0.005,
                        duration: 1000
                      },
                      angle: { value: 5, duration: 500, delay: 250 }
                    },
                    ease: 'Sine.easeInOut',
                    repeat: -1,
                    yoyo: true
                  });
                }
              }
            });
          } else {
            this.registry.set('setMessage', 'Congratulations!');
          }
        }
      }
    });
  }

  hitEdible(player, edible) {
    if (!Utils.SavedSettings.muted) {
      this.eatSnd.play();
    }

    if (player) {
      this.eatFood(player, edible);
    }

    if (edible) {
      edible.disableBody(true, true);
      this.edibleGroup.killAndHide(edible);

      if (edible._edibleObj) {
        edible._edibleObj.consumed = true;
        edible._edibleObj.sprite = undefined;
        edible._edibleObj = undefined;
      }
    }
  }

  handleTileCollision() {
    if (this.isMoving) {
      var tile = this.map.getTileAtWorldXY(
        this.player.x,
        this.player.y,
        false,
        this.cameras.main,
        this.obstacleLayer
      );

      if (tile) {
        var index = tile.index - this.baseTileId;

        var px = this.player.x - tile.pixelX;
        var py = this.player.y - tile.pixelY;

        this.canvasTexture.clear();
        this.canvasTexture.drawFrame('obstacles', index, 0, 0);
        var collision = false;

        if (this.playerHorizontalDirection === 1) {
          this.canvasTexture.getPixel(px + 1, py, this.canvasPixelColor);
          if (this.canvasPixelColor.color !== 0) {
            collision = true;
            this.player.x--;
          }
        } else if (this.playerHorizontalDirection === 2) {
          this.canvasTexture.getPixel(px - 2, py, this.canvasPixelColor);
          if (this.canvasPixelColor.color !== 0) {
            collision = true;
            this.player.x++;
          }
        }

        if (this.playerVerticalDirection === 1) {
          this.canvasTexture.getPixel(px, py + 1, this.canvasPixelColor);
          if (this.canvasPixelColor.color !== 0) {
            collision = true;
            this.player.y--;
          }
        } else if (this.playerVerticalDirection === 2) {
          this.canvasTexture.getPixel(px, py - 2, this.canvasPixelColor);
          if (this.canvasPixelColor.color !== 0) {
            collision = true;
            this.player.y++;
          }
        }

        if (collision) {
          if (!Utils.SavedSettings.muted) {
            this.bumpSnd.play();
          }

          this.tweens.killTweensOf(this.player);
          this.isMoving = false;
          this.player.body.stop();
          this.playerEyes.x = this.player.x + this.playerEyesDirection.x;
          this.playerEyes.y = this.player.y + this.playerEyesDirection.y;
        }

        return collision;
      }
    }
    return false;
  }

  movePlayer() {
    var worldPoint = this.input.activePointer.positionToCamera(
      this.cameras.main
    );

    if (!this.isMoving) {
      this.tweens.killTweensOf(this.player);
      this.player.setScale(this.playerScale);
      this.player.angle = 0;

      this.tweens.add({
        targets: this.player,
        props: {
          scaleX: { value: this.playerScale - 0.002, duration: 150 },
          scaleY: { value: this.playerScale - 0.005, duration: 150 },
          angle: { value: 15, duration: 200, delay: 50 }
        },
        ease: 'Sine.easeInOut',
        repeat: -1,
        yoyo: true
      });
    }

    this.isMoving = true;

    if (!this.handleTileCollision()) {
      var speed = 10 * (1 - this.playerScale);
      if (speed < 7) {
        speed = 7;
      }

      if (this.currentZoom <= 3) {
        speed = 20;
      }

      this.physics.moveTo(this.player, worldPoint.x, worldPoint.y, speed);
    }
  }

  doEnemyAi(enemy) {
    if (enemy && enemy.active) {
      if (enemy.x < 0) {
        enemy.x = 0;
        enemy.body.stop();
      }

      if (enemy.x > this.map.widthInPixels) {
        enemy.x = this.map.widthInPixels;
        enemy.body.stop();
      }

      if (enemy.y < 0) {
        enemy.y = 0;
        enemy.body.stop();
      }

      if (enemy.y > this.map.heightInPixels) {
        enemy.y = this.map.heightInPixels;
        enemy.body.stop();
      }

      var ai = enemy._ai;
      ai.timer++;
      if (ai.timer > 1000) {
        enemy.body.stop();
        ai.timer = 0;
        ai.state = Phaser.Math.RND.integerInRange(0, 1);
      }

      if (Phaser.Geom.Circle.Contains(this.visionCircle, enemy.x, enemy.y)) {
        enemy.body.stop();
        ai.timer = 0;
        if (enemy.scaleX > this.playerScale) {
          ai.state = 1;
        } else {
          ai.state = 2;
        }
      }

      var speed = 7 * (1 - enemy.scaleX);

      switch (ai.state) {
      case 0: //search
        if (ai.timer % 200 === 0) {
          var dir = Phaser.Math.RND.integerInRange(1, 5);
          var x = 0;
          var y = 0;
          switch (dir) {
          case 1:
            x = 100;
            break;
          case 2:
            x = -100;
            break;
          case 3:
            y = 100;
            break;
          case 4:
            y = -100;
            break;
          }
          if (dir === 5) {
            enemy.body.stop();
          } else {
            this.physics.moveTo(enemy, enemy.x + x, enemy.y + y, speed);
          }
        }

        break;

      case 1: //attack
        if (
          Phaser.Geom.Circle.Contains(this.visionCircle, enemy.x, enemy.y)
        ) {
          this.physics.moveTo(enemy, this.player.x, this.player.y, speed);
        }
        break;

      case 2: //flee
        if (
          Phaser.Geom.Circle.Contains(this.visionCircle, enemy.x, enemy.y)
        ) {
          var xp = 0;
          var yp = 0;
          if (this.player.x < enemy.x) {
            xp = enemy.x + 100;
          } else {
            xp = enemy.x - 100;
          }

          if (this.player.y < enemy.y) {
            yp = enemy.y + 100;
          } else {
            yp = enemy.y - 100;
          }
          this.physics.moveTo(enemy, xp, yp, speed);
        }
        break;
      }
    }
  }

  create() {
    this.setup();

    this.events.on('shutdown', this.shutdown, this);
    this.registry.events.on('changedata', this.registryChanged, this);
    this.registry.events.on('setdata', this.registryChanged, this);
  }

  registryChanged(parent, key, data) {
    if (key === 'muted') {
      if (data) {
        Utils.GlobalSettings.bgm.pause();
      } else {
        Utils.GlobalSettings.bgm.resume();
      }
    } else if (key === 'home') {
      if (data) {
        if (!Utils.SavedSettings.muted) {
          Utils.GlobalSettings.bgm.stop();
        }
        this.cameras.main.fade(500, 255, 255, 255);

        this.cameras.main.on(
          'camerafadeoutcomplete',
          function() {
            this.scene.start('title');
          },
          this
        );
      }
    }
  }

  update(time, delta) {
    if (this.player && this.playerEyes) {
      if (this.player.body.velocity.x > 0) {
        this.playerHorizontalDirection = 1;
        this.playerEyesDirection.x = 0.25;
      } else if (this.player.body.velocity.x < 0) {
        this.playerHorizontalDirection = 2;
        this.playerEyesDirection.x = -0.25;
      }

      if (this.player.body.velocity.y > 0) {
        this.playerVerticalDirection = 1;
        this.playerEyesDirection.y = 0.2;
      } else if (this.player.body.velocity.y < 0) {
        this.playerVerticalDirection = 2;
        this.playerEyesDirection.y = -0.2;
      }

      this.playerEyes.x = this.player.x + this.playerEyesDirection.x;
      this.playerEyes.y = this.player.y + this.playerEyesDirection.y;

      //console.log(this.player.body.velocity);

      if (this.map) {
        if (this.player.x < 0) {
          this.player.x = 0;
          this.player.body.stop();
        }

        if (this.player.x > this.map.widthInPixels) {
          this.player.x = this.map.widthInPixels;
          this.player.body.stop();
        }

        if (this.player.y < 0) {
          this.player.y = 0;
          this.player.body.stop();
        }

        if (this.player.y > this.map.heightInPixels) {
          this.player.y = this.map.heightInPixels;
          this.player.body.stop();
        }

        this.handleTileCollision();
      }

      this.handlePlayerVisibility();
    }

    if (this.enemyGroup) {
      this.enemyGroup.children.iterate(enemy => this.doEnemyAi(enemy));
    }
  }

  shutdown() {
    if (this.screenGroup) {
      this.screenGroup.destroy();
      this.screenGroup = null;
    }

    if (this.bg) {
      this.bg.destroy();
      this.bg = null;
    }

    if (this.map) {
      this.map.destroy();
      this.map = null;
    }

    this.edibleObjects = null;
    this.hsv = null;

    if (this.edibleGroup) {
      this.edibleGroup.destroy();
      this.edibleGroup = null;
    }

    if (this.enemyGroup) {
      this.enemyGroup.destroy();
      this.enemyGroup = null;
    }

    if (this.player) {
      this.player.destroy();
      this.player = null;
    }

    if (this.playerEyes) {
      this.playerEyes.destroy();
      this.playerEyes = null;
    }

    this.playerEyesDirection = null;

    if (this.canvasTexture) {
      this.canvasTexture.destroy();
      this.canvasTexture = null;
    }

    this.canvasPixelColor = null;

    this.visionCircle = null;

    this.registry.events.off('changedata', this.registryChanged, this);
    this.registry.events.off('setdata', this.registryChanged, this);
    this.events.off('shutdown', this.shutdown, this);

    console.log('kill main');
  }
}
