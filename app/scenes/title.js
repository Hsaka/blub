import Utils from 'utils/utils';

export default class TitleScene extends Phaser.Scene {
  constructor() {
    super({
      key: 'title'
    });
  }

  create() {
    Utils.load();

    this.titlebgm = this.sound.add('titlebgm');
    this.titlebgm.setVolume(0.8);
    this.titlebgm.setLoop(true);

    if (!Utils.SavedSettings.muted) {
      this.titlebgm.play();
    }

    // this.winSnd = this.sound.add('win');
    // this.winSnd.setVolume(0.2);

    this.screenGroup = this.add.container(0, 0);

    this.bg0 = this.add.image(0, 0, 'titlebg2');
    this.bg0.setOrigin(0, 0);
    this.screenGroup.add(this.bg0);

    this.bg1 = this.add.image(0, 0, 'titlebg1');
    this.bg1.setOrigin(0, 0);
    this.screenGroup.add(this.bg1);

    this.fg = this.add.image(0, 0, 'titlefg');
    this.fg.setOrigin(0, 0);
    this.screenGroup.add(this.fg);

    this.tweens.add({
      targets: this.bg1,
      alpha: 0,
      ease: 'Linear.easeOut',
      duration: 5000,
      delay: 2000
    });

    this.titleText = this.add.image(120, 170, 'titleText');
    this.screenGroup.add(this.titleText);
    this.titleText.alpha = 0;

    this.tweens.add({
      targets: this.titleText,
      alpha: 1,
      ease: 'Linear.easeOut',
      duration: 1500,
      delay: 9000
    });

    this.message = this.add.bitmapText(
      this.titleText.x - 70,
      this.titleText.y + 40,
      'kenpixel',
      '- Click to Start -',
      20
    );
    this.message.setOrigin(0);
    this.message.alpha = 0;

    this.tweens.add({
      targets: this.message,
      alpha: 1,
      ease: 'Linear.easeOut',
      duration: 1000,
      delay: 10000,
      onComplete: () => {
        this.tweens.add({
          targets: this.credits,
          alpha: 1,
          y: Utils.GlobalSettings.height - 20,
          ease: 'Sine.easeOut',
          duration: 2500,
          delay: 100,
          yoyo: true,
          loop: -1,
          onLoop: () => {
            this.creditIndex++;
            if (this.creditIndex >= this.creditText.length) {
              this.creditIndex = 0;
            }
            this.credits.setText(this.creditText[this.creditIndex]);
          }
        });
      }
    });

    this.creditIndex = 0;
    this.creditText = [
      'Game Design: lombardus',
      'Code: hsaka',
      'Music: syncopika',
      'Music: Daniel Stephens (scribe)',
      'Artwork: ansimuz',
      'Artwork: shohan4556',
      'Artwork: rubberduck',
      'Artwork: bluecarrot16'
    ];

    this.credits = this.add.bitmapText(
      10,
      Utils.GlobalSettings.height,
      'kenpixel',
      this.creditText[0],
      20
    );
    this.credits.alpha = 0;

    this.input.on('pointerdown', pointer => {
      if (!Utils.SavedSettings.muted) {
        this.titlebgm.stop();
      }

      this.cameras.main.fade(500, 255, 255, 255);

      this.cameras.main.on(
        'camerafadeoutcomplete',
        function() {
          this.scene.start('main');
        },
        this
      );
    });

    if (this.scene.isSleeping('gameUI')) {
      this.scene.wake('gameUI', { showHome: false });
      this.scene.bringToTop('gameUI');
    } else if (!this.scene.isActive('gameUI')) {
      this.scene.launch('gameUI', { showHome: false });
      this.scene.bringToTop('gameUI');
    }
    this.registry.set('showHome', false);
    this.registry.set('setMessage', '');

    this.events.on('shutdown', this.shutdown, this);
    this.registry.events.on('changedata', this.registryChanged, this);
    this.registry.events.on('setdata', this.registryChanged, this);
  }

  registryChanged(parent, key, data) {
    if (key === 'muted') {
      if (data) {
        this.titlebgm.pause();
      } else {
        this.titlebgm.play();
      }
    }
  }

  shutdown() {
    if (this.screenGroup) {
      this.screenGroup.destroy();
      this.screenGroup = null;
    }

    if (this.bg1) {
      this.bg1.destroy();
      this.bg1 = null;
    }

    this.registry.events.off('changedata', this.registryChanged, this);
    this.registry.events.off('setdata', this.registryChanged, this);
    this.events.off('shutdown', this.shutdown, this);

    console.log('kill title');
  }
}
