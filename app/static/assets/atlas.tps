<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.6.2</string>
        <key>fileName</key>
        <string>E:/dev/sync/wip/blub/app/static/assets/atlas.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>phaser-json-hash</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>sprites-{n}.json</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <true/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <true/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">raw/blub/blub1.png</key>
            <key type="filename">raw/blub/blub2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>18,16,37,32</rect>
                <key>scale9Paddings</key>
                <rect>18,16,37,32</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">raw/eyes.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,3,17,5</rect>
                <key>scale9Paddings</key>
                <rect>8,3,17,5</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">raw/plants/plants1.png</key>
            <key type="filename">raw/plants/plants10.png</key>
            <key type="filename">raw/plants/plants11.png</key>
            <key type="filename">raw/plants/plants12.png</key>
            <key type="filename">raw/plants/plants13.png</key>
            <key type="filename">raw/plants/plants14.png</key>
            <key type="filename">raw/plants/plants15.png</key>
            <key type="filename">raw/plants/plants16.png</key>
            <key type="filename">raw/plants/plants17.png</key>
            <key type="filename">raw/plants/plants18.png</key>
            <key type="filename">raw/plants/plants19.png</key>
            <key type="filename">raw/plants/plants2.png</key>
            <key type="filename">raw/plants/plants20.png</key>
            <key type="filename">raw/plants/plants21.png</key>
            <key type="filename">raw/plants/plants22.png</key>
            <key type="filename">raw/plants/plants23.png</key>
            <key type="filename">raw/plants/plants24.png</key>
            <key type="filename">raw/plants/plants25.png</key>
            <key type="filename">raw/plants/plants26.png</key>
            <key type="filename">raw/plants/plants27.png</key>
            <key type="filename">raw/plants/plants28.png</key>
            <key type="filename">raw/plants/plants29.png</key>
            <key type="filename">raw/plants/plants3.png</key>
            <key type="filename">raw/plants/plants30.png</key>
            <key type="filename">raw/plants/plants31.png</key>
            <key type="filename">raw/plants/plants32.png</key>
            <key type="filename">raw/plants/plants33.png</key>
            <key type="filename">raw/plants/plants34.png</key>
            <key type="filename">raw/plants/plants35.png</key>
            <key type="filename">raw/plants/plants36.png</key>
            <key type="filename">raw/plants/plants37.png</key>
            <key type="filename">raw/plants/plants38.png</key>
            <key type="filename">raw/plants/plants39.png</key>
            <key type="filename">raw/plants/plants4.png</key>
            <key type="filename">raw/plants/plants40.png</key>
            <key type="filename">raw/plants/plants41.png</key>
            <key type="filename">raw/plants/plants42.png</key>
            <key type="filename">raw/plants/plants43.png</key>
            <key type="filename">raw/plants/plants44.png</key>
            <key type="filename">raw/plants/plants45.png</key>
            <key type="filename">raw/plants/plants46.png</key>
            <key type="filename">raw/plants/plants47.png</key>
            <key type="filename">raw/plants/plants48.png</key>
            <key type="filename">raw/plants/plants49.png</key>
            <key type="filename">raw/plants/plants5.png</key>
            <key type="filename">raw/plants/plants50.png</key>
            <key type="filename">raw/plants/plants51.png</key>
            <key type="filename">raw/plants/plants52.png</key>
            <key type="filename">raw/plants/plants53.png</key>
            <key type="filename">raw/plants/plants54.png</key>
            <key type="filename">raw/plants/plants55.png</key>
            <key type="filename">raw/plants/plants56.png</key>
            <key type="filename">raw/plants/plants57.png</key>
            <key type="filename">raw/plants/plants58.png</key>
            <key type="filename">raw/plants/plants59.png</key>
            <key type="filename">raw/plants/plants6.png</key>
            <key type="filename">raw/plants/plants7.png</key>
            <key type="filename">raw/plants/plants8.png</key>
            <key type="filename">raw/plants/plants9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,16,16</rect>
                <key>scale9Paddings</key>
                <rect>8,8,16,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">raw/systembuttons/systembuttons0.png</key>
            <key type="filename">raw/systembuttons/systembuttons1.png</key>
            <key type="filename">raw/systembuttons/systembuttons10.png</key>
            <key type="filename">raw/systembuttons/systembuttons11.png</key>
            <key type="filename">raw/systembuttons/systembuttons2.png</key>
            <key type="filename">raw/systembuttons/systembuttons3.png</key>
            <key type="filename">raw/systembuttons/systembuttons4.png</key>
            <key type="filename">raw/systembuttons/systembuttons5.png</key>
            <key type="filename">raw/systembuttons/systembuttons6.png</key>
            <key type="filename">raw/systembuttons/systembuttons7.png</key>
            <key type="filename">raw/systembuttons/systembuttons8.png</key>
            <key type="filename">raw/systembuttons/systembuttons9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>13,13,25,25</rect>
                <key>scale9Paddings</key>
                <rect>13,13,25,25</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>raw/systembuttons</filename>
            <filename>raw/blub</filename>
            <filename>raw/eyes.png</filename>
            <filename>raw/plants</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
